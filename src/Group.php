<?php

namespace Kgabryel\Routing;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Loader\Configurator\RoutingConfigurator;
use Symfony\Component\Routing\Route;

class Group
{
    public const URL_SEPARATOR = '/';
    public const COLLECTION_PATH = self::URL_SEPARATOR;
    public const SINGLE_ELEMENT_PATH = self::COLLECTION_PATH . '{id}';
    public const CREATE_PATH = self::URL_SEPARATOR . 'create';
    public const EDIT_PATH = self::SINGLE_ELEMENT_PATH . '/edit';
    public const ACTION_INDEX = 'index';
    public const ACTION_CREATE = 'create';
    public const ACTION_STORE = 'store';
    public const ACTION_SHOW = 'show';
    public const ACTION_EDIT = 'edit';
    public const ACTION_UPDATE = 'update';
    public const ACTION_MODIFY = 'modify';
    public const ACTION_DESTROY = 'destroy';
    protected RoutingConfigurator $configurator;
    protected string $namePrefix;
    protected string $pathPrefix;
    protected string $controller;

    public function __construct(
        RoutingConfigurator $configurator, string $namePrefix = '', string $pathPrefix = ''
    )
    {
        $this->configurator = $configurator;
        $this->namePrefix = $namePrefix;
        $this->pathPrefix = trim($pathPrefix, self::URL_SEPARATOR);
        $this->controller = '';
    }

    public function setController(string $controller): self
    {
        $this->controller = $controller;
        return $this;
    }

    public function add(string $name, Route $route, Info $info, array $requirements = []): self
    {
        $route->setMethods($info->getMethods());
        $this->configurator->add(
            $this->namePrefix . '.' . $name,
            $this->clearPath($route->getPath())
        )
            ->controller($this->chooseControllerName($info))
            ->methods($info->getMethods())
            ->requirements($requirements);
        return $this;
    }

    public function addIndex(): self
    {
        $this->add(
            self::ACTION_INDEX,
            new Route(self::COLLECTION_PATH),
            new Info([Request::METHOD_GET], self::ACTION_INDEX)
        );
        return $this;
    }

    public function addCreate(): self
    {
        $this->add(
            self::ACTION_CREATE,
            new Route(self::CREATE_PATH),
            new Info([Request::METHOD_GET], self::ACTION_CREATE)
        );
        return $this;
    }

    public function addStore(): self
    {
        $this->add(
            self::ACTION_STORE,
            new Route(self::COLLECTION_PATH),
            new Info([Request::METHOD_POST], self::ACTION_STORE)
        );
        return $this;
    }

    public function addShow(): self
    {
        $this->add(
            self::ACTION_SHOW,
            new Route(self::SINGLE_ELEMENT_PATH),
            new Info([Request::METHOD_GET], self::ACTION_SHOW),
            ['id' => '\d+']
        );
        return $this;
    }

    public function addEdit(): self
    {
        $this->add(
            self::ACTION_EDIT,
            new Route(self::EDIT_PATH),
            new Info([Request::METHOD_GET], self::ACTION_EDIT),
            ['id' => '\d+']
        );
        return $this;
    }

    public function addUpdate(): self
    {
        $this->add(
            self::ACTION_UPDATE,
            new Route(self::SINGLE_ELEMENT_PATH),
            new Info([Request::METHOD_PUT], self::ACTION_UPDATE),
            ['id' => '\d+']
        );
        return $this;
    }

    public function addModify(): self
    {
        $this->add(
            self::ACTION_MODIFY,
            new Route(self::SINGLE_ELEMENT_PATH),
            new Info([Request::METHOD_PATCH], self::ACTION_MODIFY),
            ['id' => '\d+']
        );
        return $this;
    }

    public function addDestroy(): self
    {
        $this->add(
            self::ACTION_DESTROY,
            new Route(self::SINGLE_ELEMENT_PATH),
            new Info([Request::METHOD_DELETE], self::ACTION_DESTROY),
            ['id' => '\d+']
        );
        return $this;
    }

    private function chooseControllerName(Info $info): string
    {
        $controllerName = $info->getController() === '' ? $this->controller : $info->getController(
        );
        return sprintf('%s::%s', $controllerName, $info->getAction());
    }

    private function clearPath(string $path): string
    {
        return trim(
            $this->pathPrefix . self::URL_SEPARATOR . trim(
                $path,
                self::URL_SEPARATOR
            ),
            self::URL_SEPARATOR
        );
    }
}